# CHANGELOG



## v0.2.1 (2024-03-16)

### Fix

* fix(deploy): correct tab in gitlab-ci ([`06d9b63`](https://gitlab.com/guillerminamayorca/my-package/-/commit/06d9b631bf440ef48e242bd5fe1240618319501b))

* fix(deploy): add deploy pipeline ([`5d5637a`](https://gitlab.com/guillerminamayorca/my-package/-/commit/5d5637a114015d2824a5ef4ad1e9d6d877d0d7c3))


## v0.2.0 (2024-03-16)

### Feature

* feat(addition): add addition feature ([`fa9a5b6`](https://gitlab.com/guillerminamayorca/my-package/-/commit/fa9a5b67fd56fec9988a272d3ce331cfff0c64fe))


## v0.1.0 (2024-03-16)

### Feature

* feat(sr): add semantic release cd pipeline ([`be72634`](https://gitlab.com/guillerminamayorca/my-package/-/commit/be72634cdb707d5b260ee46c35229777820c9257))

### Unknown

* add semantic-release config ([`359aafc`](https://gitlab.com/guillerminamayorca/my-package/-/commit/359aafc2ffb4087ec5e70ac36d01313840065946))

* Update pyproject.toml ([`4ce0778`](https://gitlab.com/guillerminamayorca/my-package/-/commit/4ce07785091b1e203a48fd4fb5b54e28c02cb0af))

* Add pyproject.toml to init project ([`215e221`](https://gitlab.com/guillerminamayorca/my-package/-/commit/215e2218f73bbf6a426184090f410f492c52c6c9))

* Update .gitlab-ci.yml file ([`cec07a3`](https://gitlab.com/guillerminamayorca/my-package/-/commit/cec07a3c71609fdc9afef6ca3eeb3844e816d213))

* Add first pipeline ([`32a5404`](https://gitlab.com/guillerminamayorca/my-package/-/commit/32a54044d42233564d59645a8de00a0801c77d41))

* Initial commit ([`19b015e`](https://gitlab.com/guillerminamayorca/my-package/-/commit/19b015e39208dee1a0d97d9adf04be2ab92027da))
